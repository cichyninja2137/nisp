#include <stdio.h>
int sum(int,int);
int sub(int,int);
int mul(int,int);
int min(int,int);
int max(int,int);
int main()
{
    int a=1;
    int b=2;
    printf("%d+%d=%d\n",a,b,sum(a,b));
    return 0;
}

int sum(int a,int b){
    return a+b;
}
int sub(int a,int b){
    return a-b;
}
int mul(int a, int b){
    return a*b;
}
int min(int a, int b){
    if(a>b){
        return b;
    }
    else {
        return a;
    }
}
int max(int a, int b){
    if(a>b){
        return a;
    }
    else {
        return b;
    }
}
